const path = require('path')

const rules = [
  {
    test: /\.scss$/,
    use: [
      'vue-style-loader',
      'css-loader',
      'sass-loader',
      {
        loader: 'postcss-loader',
        options: {
          config: {
            path: path.resolve(__dirname)
          }
        }
      }
    ]
  }
]

module.exports = async ({ config }) => {
  rules.forEach((rule) => {
    config.module.rules.push(rule)
  })

  return config
}
