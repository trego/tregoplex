import { storiesOf } from '@storybook/vue';

import TpSiteHeader from '../../components/TpSiteHeader'
import markdown from './TpSiteHeader.md'

storiesOf('TpSiteHeader', module)
  .add(
    'Basic',
    () => ({
      components: {
        TpSiteHeader
      },
      template: `
        <TpSiteHeader />
      `
    }),
    {
      notes: { markdown }
    }
  )