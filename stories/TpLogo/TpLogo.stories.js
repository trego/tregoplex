import { storiesOf } from '@storybook/vue'

import TpLogo from '../../components/TpLogo'
import markdown from './TpLogo.md'

storiesOf('TpLogo', module)
  .add(
    'Basic',
    () => ({
      components: {
        TpLogo
      },
      template: `
        <TpLogo>
          <a href="#">
            Hype Deals.
          </a>
        </TpLogo>
      `
    }),
    {
      notes: { markdown }
    }
  )